import React from 'react';
import { connect } from 'react-redux';

import App from './App';
import Movie from './pages/Movie';
import Home from './pages/Home';


import { Route, Switch } from 'react-router-dom';
import { ConnectedRouter } from 'react-router-redux';

class Router extends React.Component {

    render(){
        return (
        <ConnectedRouter history={this.props.history}>
          <App>
            <Switch>
              <Route exact path="/" component={Home}></Route>
              <Route path="/movie/:title/:id" component={Movie}></Route>
            </Switch>
          </App>
        </ConnectedRouter>
        );
    }
}

function mapStateToProps(state, ownProps){
  return {
    user: state.user
  }
}

export default connect(mapStateToProps)(Router);