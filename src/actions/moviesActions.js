export function loadMovies(movies){
	return {
		type: 'LOAD_MOVIES', 
		movies
	}
}

export function searchMovie(title, id){
	return {
		type: 'SEARCH_MOVIE', 
		title,
		id
	}
}