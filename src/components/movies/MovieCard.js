import React from 'react';

import { Card, CardMedia, CardActions} from 'material-ui/Card';
import FlatButton from 'material-ui/FlatButton';
import { Link } from 'react-router-dom';
import {grey900} from 'material-ui/styles/colors';

import FadeAndScale from '../animations/FadeAndScale';


export default class MovieCard extends React.Component{

  render(){
    return(
      <FadeAndScale className="col-xs-12 col-sm-4"  in={this.props.in}>
        <div style={{'paddingBottom': '20px'}}>
          <Card style={{'backgrounColor': grey900}}>
            <CardMedia>
              <img  alt="img" src={this.props.movie.image_large}/>
            </CardMedia> 
            <CardActions style={{'textAlign': 'right', 'backgroundColor':grey900}}>
                <Link to={"/movie/"+ this.props.movie.title_uri + "/" + this.props.movie.id} >
                    <FlatButton primary={true} label="Ver más" />
                </Link>
            </CardActions>
          </Card>
        </div>
      </FadeAndScale>
    );
  }
}
