import React from 'react';

import AppBar from 'material-ui/AppBar';
import {grey900} from 'material-ui/styles/colors';



export default class MyAppBar extends React.Component {

    title(){
        return (
            <span style={{'cursor':'pointer', 'textTransform': 'capitalize', 'color':'red'}}>
                {'Claro-video'}
            </span>
        );
    }

    render(){
        return (
            <AppBar
                title={this.title()}
                style={{'backgroundColor': grey900}}
                showMenuIconButton={false}
                onTitleTouchTap={this.props.goHome}
            />
        );
    }
}