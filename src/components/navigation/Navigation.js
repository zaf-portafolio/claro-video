import React from 'react';
import { push } from 'react-route-redux';

import MyAppBar from './MyAppBar';


class Navigation extends React.Component{
	constructor(props){
		super(props);
		this.goHome = this.goHome.bind(this);
	}

	goHome(){
		this.props.dispatch(push('/'));
	}

	render(){
		return <MyAppBar goHome={this.goHome} />
	}
}
