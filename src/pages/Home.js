import React from 'react';
import TransitionGroup from 'react-transition-group/TransitionGroup';
import {grey800} from 'material-ui/styles/colors';
import Container from '../components/Container';
import MovieCard from '../components/movies/MovieCard';

import {getMovies} from '../requests/movies';
import  TextField  from 'material-ui/TextField';


export default class Home extends React.Component {

    constructor(props){
        super(props)
        this.state={
            movies : [],
            moviesSearch : []
        }
        this.filtrar = this.filtrar.bind(this);
    }


	filtrar(event) {
        let movieFliterSearch = [];
		let title = event.target.value;

        if(title.length){
            this.state.moviesSearch.map((movie, index)=>{
                if(movie.title.toLowerCase().includes(title.toLowerCase())){
                    movieFliterSearch.push(movie);
                }
                return true;
            });
            console.log(movieFliterSearch);
            this.setState({
                movies: movieFliterSearch
            })
        }else{
            this.state.moviesSearch.map((movie, index)=>{
                movieFliterSearch.push(movie);
                return true;
            });
            console.log(movieFliterSearch);
            this.setState({
                movies: movieFliterSearch
            })
        }
	}

	componentDidMount() {
        getMovies().then(jsonM => {
            this.setState({
                movies: jsonM.response.groups,
                moviesSearch: jsonM.response.groups
            })
        });
	}

    movies(){
        return this.state.movies.map((movie,index)=>{
            return(
                <MovieCard movie={movie} index={index} key={index}/>
            );
        })
    }

    render(){
        return (
        <div style={{'backgroundColor': grey800, 'padding': '50px', color: 'white'}}>
            <Container>
                <div className="row">
                    <div className="col-xs-12">
                        <div className="search">
                            <TextField
                            floatingLabelStyle={{'color': "#FFF"}}
                            floatingLabelFocusStyle={{'color': "#FFF"}}
                            underlineFocusStyle={{'color': "#FFF"}}
                            floatingLabelText="Buscar"
                            style={{'color':'#FFF'}}
                            ref="busqueda"
                            name="busqueda"
                            onChange={this.filtrar}
                            
                            />
                        </div>
                        <TransitionGroup className="row" timeout={400}>
                            {this.movies()}
                        </TransitionGroup>
                    </div>
                </div>
            </Container>
        </div>
        );
    }
}