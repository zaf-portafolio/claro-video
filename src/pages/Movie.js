import React from 'react';
import  Card  from 'material-ui/Card';
import { withRouter } from 'react-router-dom';
import {grey900} from 'material-ui/styles/colors';

import Container from '../components/Container';
import {getMovie} from '../requests/movies';

class Movie extends React.Component {

    constructor(props){
        super(props);
        this.state={
            movie: {}
        }

        const title = props.match.params.title;
        const id = props.match.params.id;
        this.loadMovie(title, id);
    }

    loadMovie(title, id){
        getMovie(id).then(json=>{
            this.setState({
                movie : json.response.group.common
            })
        })
    }

    render(){
        const {movie} = this.state; 
        return (
            <div className="Movie-container" style={{'backgroundImage':'url('+movie.image_background+')'}}>
                <header className="Movie-cover">

                </header>
                <Container>
                    <div className="row">
                        <div className="col-xs-12">
                            <Card className="Movie-card" style={{'backgroundColor':grey900}}>
                                <div className="row">
                                    <div className="col-xs-12 col-sm-4">
                                        <img src={movie.image_medium} style={{'maxWidth':'100%'}} alt={movie.image_medium_alt}/>
                                    </div>
                                    <div className="col-xs" style={{'color':'white'}}>
                                        <h1>{movie.title}</h1>
                                        <address>{movie.duration}</address>
                                        <p>{movie.large_description}</p>
                                    </div>
                                </div>
                            </Card>
                        </div>
                    </div>
                </Container>
            </div>
        ) ;
    }
}

export default withRouter(Movie);