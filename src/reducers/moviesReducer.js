export default function moviesReducer(state = [1], action){
	
	switch(action.type){
		case 'LOAD_MOVIES':
			return action.movies;

		case 'SEARCH_MOVIE':
			return Object.assign({}, state,{
				title: action.movies.title,
				id: action.movies.id
			});

		default:
			return state;
	}
}