function getMovies(){
  return fetch("https://mfwkweb-api.clarovideo.net//services/content/list?api_version=v5.8&authpn=webclient&authpt=tfg1h3j4k6fd7&format=json&region=mexico&device_id=web&device_category=web&device_model=web&device_type=web&device_manufacturer=generic&HKS=tcd5lcsj85n28v7dsas6rvsb66&quantity=80&order_way=DESC&order_id=200&level_id=GPS&from=0&node_id=9870").then(data =>{
    return data.json();
  }).catch(console.log);
}

function getMovie(id){
  let url = "https://mfwkweb-api.clarovideo.net/services/content/data?api_version=v5.8&authpn=webclient&authpt=tfg1h3j4k6fd7&format=json&region=mexico&device_id=web&device_category=web&device_model=web&device_type=web&device_manufacturer=generic&HKS=tcd5lcsj85n28v7dsas6rvsb66&group_id="+id;

  return fetch(url).then(data =>{
    return data.json();
  }).catch(console.log);
}

export {getMovies, getMovie}
